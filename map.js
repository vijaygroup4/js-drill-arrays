//map function
function map(elements, callback) {
  let newArray = [];
  for (let index = 0; index < elements.length; index++) {
    newArray.push(callback(elements[index], index));
  }
  return newArray;
}

//exporting the above function
module.exports = map;
