//flatten function
function flatten(elements) {
  let flattenedArray = [];

  function recursiveFlatten(array) {
    for (let index = 0; index < array.length; index++) {
      //checking if the element is array or not
      if (Array.isArray(array[index]) == true) {
        recursiveFlatten(array[index]);
      } else {
        flattenedArray.push(array[index]);
      }
    }
  }

  recursiveFlatten(elements);
  return flattenedArray;
}

//exporting the above function
module.exports = flatten;
