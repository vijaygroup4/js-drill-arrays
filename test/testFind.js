//importing the items
const items = require("./items");

//importing the find function
const find = require("../find");

function testFind() {
  let value = find(items, (item) => {
    return item > 0;
  });
  if (value) {
    console.log(value);
  } else {
    console.log("No positive values");
  }
}

testFind();
