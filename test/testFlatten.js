//declaring nestedArray
const nestedArray = [1, [2], [[3]], [[[4]]]];

// importing flatten function from flatten.js
const flatten = require("../flatten");

function testFlatten() {
  let flattenArray = flatten(nestedArray);
  console.log(flattenArray);
}

testFlatten();
