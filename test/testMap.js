//importing the items
const items = require("./items");

//importing the map function
const map = require("../map");

function testMap() {
  const doubleValues = map(items, (item, index) => {
    return item * 2;
  });
  console.log(doubleValues);
}

testMap();
