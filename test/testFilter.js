//importing items
const items = require("./items");

//importing the filter function
const filter = require("../filter");

function testFilter() {
  const evenElements = filter(items, (element) => {
    return element % 2 == 0;
  });
  console.log(`Even elements are ${evenElements}`);
  const oddElements = filter(items, (element) => {
    return element % 2 != 0;
  });
  console.log(`Odd elements are ${oddElements}`);
}

testFilter();
