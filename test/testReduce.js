//importing the items
const items = require("./items");

//importing the reduce function
const reduce = require("../reduce");

function testReduce() {
  let productArray = reduce(
    items,
    (accumulator, currentValue) => {
      return accumulator * currentValue;
    },
    1
  );

  console.log(`Product of items is ${productArray}`);
}

testReduce();
