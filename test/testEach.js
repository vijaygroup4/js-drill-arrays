//importing items array
const items = require("./items");

//importing each function
const each = require("../each");

function testEach() {
  each(items, (item, index) => {
    console.log(`${index}: ${item}`);
  });
}

testEach();
