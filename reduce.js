//reduce function
function reduce(elements, callback, startingValue) {
  let accumulator = 0;
  if (startingValue !== undefined) {
    accumulator = startingValue;
  } else {
    accumulator = elements[0];
  }
  for (let index = 0; index < elements.length; index++) {
    accumulator = callback(accumulator, elements[index]);
  }
  return accumulator;
}

//exporting the above function
module.exports = reduce;
