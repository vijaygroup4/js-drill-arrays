//each funtion
function each(elements, callback) {
  for (let index = 0; index < elements.length; index++) {
    callback(elements[index], index);
  }
}

//exporting the above function
module.exports = each;
