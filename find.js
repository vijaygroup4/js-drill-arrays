//find function
function find(elements, callback) {
  for (let index = 0; index < elements.length; index++) {
    if (callback(elements[index]) == true) {
      return elements[index];
    }
  }

  //returning undefined if no element passes the test
  return undefined;
}

//exporting the above function
module.exports = find;
