//filter function
function filter(elements, callback) {
  let filteredElements = [];
  for (let index = 0; index < elements.length; index++) {
    if (callback(elements[index]) == true) {
      filteredElements.push(elements[index]);
    }
  }
  return filteredElements;
}

//exporting the above function
module.exports = filter;
